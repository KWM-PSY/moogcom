# ===========================================================
# GIT CONTROL (part of the MoogCom module)
# ===========================================================
# included functions:
# - set_branches 
#
# ===========================================================
# Copyright (C) 2020 Matthias Ertl, University of Bern,
# matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


function set_branches()
  # checkout master in MoogCom
  path2Home = homedir()
  path2MoogCom = "/julia/MoogCom/"
  cd("$path2Home/$path2MoogCom")
  run(`git checkout master`)

  # checkout master in Exp_helper
  cd("../Exp_helper/")
  run(`git checkout master`)

  # checkout master in Questionnaires
  cd("../Questionnaires")
  run(`git checkout master`)
end
