# ===========================================================
# JOYSTICK (part of the MoogCom module)
# ===========================================================
# included functions:
# - login_joystick
#
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
function login_joystick(moog_port, mailto, report_resolution, const_velocity = 0.1, rot_center = [0.0,0.0,0.0])
  # is joystick conected
  println("make sure the joystick is connected!")
  # send login request to MOOG
  if moog_port.sts1!=MOOG.SSTATE_OFF
    error("must be in off-state in order to login")
  end

  # creating the login message in one command/one line didn't work, doing it in two strangely      does work.
  loginmsg_part1 = @sprintf("%d,%d",MOOG.SESSION_JOYSTICK,moog_port.remport)
  loginmsg_part2 = @sprintf("%f,%f,%f,%f",const_velocity,rot_center[1],rot_center[2],rot_center[3])
  loginmsg = loginmsg_part1*","*loginmsg_part2
  println(loginmsg)

  MOOG.sendmsg(moog_port,MOOG.MSG_LOGIN,loginmsg)

  # process the MOOG's response to login or engage request
  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      # check if a button is pressed, if it is park
      if(recv[2]==MOOG.MSG_DPIN_CHANGE)
        if(recv[3][1]==MoogCom.BTN_LBL_L && recv[3][2]=="1")
          MoogCom.park(moog_port,reportmsg)
          break
        elseif(recv[3][1]==MoogCom.BTN_LBL_R && recv[3][2]=="1")
          MoogCom.park(moog_port, mailto, report_resolution)
          break
        end
      end
    end
  end
end
