# ===========================================================
# STIMULUS SERVER (part of the MoogCom module)
# ===========================================================
# included audio function definitions:
# - open_audio
# - play_audio
# - stop_audio
#
# included stills function definitions:
# - open_image
# - display_image
# - display_color
# - display_draw
# - display_hide
#
# included movie function definitions:
# - open_movie
# - display_movie
#  
# included GVS function definitions:
# - open_GVS
# - play_GVS
# - stop_GVS
# ===========================================================
# Copyright (C) 2020 Daniel Fitze, Gerda Wyssen and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, gerda.wyssen@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================

# ===========================================================
# Audio functions
# ===========================================================
function open_audio(moog_port; n_channels, disp_INFO = true)
  println("[ INFO: setting up audio ]")
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_OPEN_AUDIO, @sprintf("%d",n_channels))

  while(true)
    recv=MOOG.loop(moog_port)

    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_AUDIO_NAK)
        error("[ INFO: Moog did not ack the audio ($recv[3]) ]")
      elseif(recv[2]==MOOG.MSG_AV_AUDIO_ACK)
        if disp_INFO
          println("[ INFO: Audio Acknowledged ]")
        end
        return
      else
        if disp_INFO
          println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
        end
      end
    end
  end
end

function play_audio(moog_port, sound, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_AUDIO_PLAY,@sprintf("%s,%d,%f,%d", sound.filename, sound.channel, sound.volume, sound.repetitions))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_AUDIO_ACK)
        if disp_INFO
          println("[ INFO: AV_AUDIO_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_AUDIO_NAK)
        println(@sprintf("%d,%s,%d,%f", MOOG.MSG_AV_AUDIO_PLAY, sound.filename, sound.channel, sound.volume))
        error("[ INFO: nack audio $(recv) ]")
      end
    end
  end
end

function stop_audio(moog_port, sound, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_AUDIO_STOP,@sprintf("%d", sound.channel))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_AUDIO_ACK)
        if disp_INFO
          println("[ INFO: AV_AUDIO_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_AUDIO_NAK)
        println(@sprintf("%d,%s,%d,%f", MOOG.MSG_AV_AUDIO_STOP, sound.filename, sound.channel, sound.volume))
        error("[ INFO: nack audio $(recv) ]")
      end
    end
  end
end

# ===========================================================
# Stills functions
# ===========================================================
function open_image(moog_port, parameters, disp_INFO = true)
  println("[ INFO: setting up stills ]")

  mg = ""

  for x = 1:length(parameters.x_pos_virt)

    if parameters.screen_type[x] == "F"
      mg = mg * "F" * "#" * string(parameters.x_pos_virt[x]) * "#" * string(parameters.y_pos_virt[x]) * "#" * string(parameters.z_pos_virt[x]) * "#" * string(parameters.yaw_virt[x]) * "#" * string(parameters.pitch_virt[x]) * "#" * string(parameters.roll_virt[x]) * "#" * string(parameters.width_virt[x]) * "#" * string(parameters.height_virt[x])
    elseif parameters.screen_type[x] == "SE" || parameters.screen_type[x] == "SI"
      mg = mg * parameters.screen_type[x] * "#" * string(parameters.x_pos_virt[x]) * "#" * string(parameters.y_pos_virt[x]) * "#" * string(parameters.z_pos_virt[x]) * "#" * string(parameters.radius_virt[x]) 
    else
      println("screen_type has to be set to F/SE/SI, found $parameters.screen_type")
    end

    if x < length(parameters.x_pos_virt)
      mg = mg * "|"
    end
  end

  MOOG.sendmsg(moog_port, MOOG.MSG_AV_OPEN_STILLS, @sprintf("%d,%d,%s|%d#%d#%d#%d,%s",parameters.x_res, parameters.y_res, parameters.visor, parameters.x_pos_offset, parameters.y_pos_offset, parameters.width, parameters.height, mg))

  println(mg)
  while(true)
    recv=MOOG.loop(moog_port)

    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_STILLS_NAK)
        error("[ INFO: Moog did not ack the stills ($recv[3]) ]")
      elseif(recv[2]==MOOG.MSG_AV_STILLS_ACK)
        if disp_INFO
          println("[ INFO: Stills Acknowledged ]")
        end
        return
      else
        println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
      end
    end
  end
end

# display_image allows to set a certain screen to visible.
function display_image(moog_port, still, screen_nb, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_STILLS_STILL,@sprintf("%s,%d", still, screen_nb))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_STILLS_ACK)
        if disp_INFO
          println("[ INFO: AV_STILLS_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_STILLS_NAK)
        error("[ INFO: nack still$(recv) ]")
      end
    end
  end
end

# display_color allows to set the color of a certain screen to a color of choice.
function display_color(moog_port, color, screen_nb,disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_STILLS_FILL_RGB,@sprintf("%d,%d,%d,%d", color[1], color[2], color[3], screen_nb))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_STILLS_ACK)
        if disp_INFO
          println("[ INFO: AV_STILLS_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_STILLS_NAK)
        error("[ INFO: nack still$(recv) ]")
      end
    end
  end
end

# display_draw draws a screen e.g. after an update
function display_draw(moog_port, screen_nb, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_STILLS_ACTIVATE,@sprintf("%d", screen_nb))
end

# display_hide sets a screen to invisible
function display_hide(moog_port, screen_nb, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_STILLS_DEACTIVATE,@sprintf("%d", screen_nb))
end

# ===========================================================
# Movie functions
# ===========================================================
function open_movie(moog_port, parameters, disp_INFO = true)
  println("[ INFO: setting up movies ]")

  mg = ""

  for x = 1:length(parameters.x_pos_virt)

    if parameters.screen_type[x] == "F"
      mg = mg * "F" * "#" * string(parameters.x_pos_virt[x]) * "#" * string(parameters.y_pos_virt[x]) * "#" * string(parameters.z_pos_virt[x]) * "#" * string(parameters.yaw_virt[x]) * "#" * string(parameters.pitch_virt[x]) * "#" * string(parameters.roll_virt[x]) * "#" * string(parameters.width_virt[x]) * "#" * string(parameters.height_virt[x])
    elseif parameters.screen_type[x] == "SE" || parameters.screen_type[x] == "SI"
      mg = mg * parameters.screen_type[x] * "#" * string(parameters.x_pos_virt[x]) * "#" * string(parameters.y_pos_virt[x]) * "#" * string(parameters.z_pos_virt[x]) * "#" * string(parameters.radius_virt[x]) 
    else
      println("screen_type has to be set to F/SE/SI, found $parameters.screen_type")
    end

    if x < length(parameters.x_pos_virt)
      mg = mg * "|"
    end
  end

  MOOG.sendmsg(moog_port, MOOG.MSG_AV_OPEN_MOVIES, @sprintf("%d,%d,%s|%d#%d#%d#%d,%s",parameters.x_res, parameters.y_res, parameters.visor, parameters.x_pos_offset, parameters.y_pos_offset, parameters.width, parameters.height, mg))

  println(mg)
  while(true)
    recv=MOOG.loop(moog_port)

    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_MOVIES_NAK)
        error("[ INFO: Moog did not ack the movies ($recv[3]) ]")
      elseif(recv[2]==MOOG.MSG_AV_MOVIES_ACK)
        if disp_INFO
          println("[ INFO: Movies Acknowledged ]")
        end
        return
      else
        println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
      end
    end
  end
end

# display_movie allows to set a certain screen to visible.
function display_movie(moog_port, movie, screen_nb, disp_INFO = true, loop_INFO = 1, interrupt_INFO = 1)

  MOOG.sendmsg(moog_port, MOOG.MSG_AV_MOVIES_MOVIE,@sprintf("%s,%d,%d,%d", movie, screen_nb, loop_INFO, interrupt_INFO))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_MOVIES_ACK)
        if disp_INFO
          println("[ INFO: AV_MOVIES_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_MOVIES_NAK)
        error("[ INFO: nack movie$(recv) ]")
      end
    end
  end
end

# ===========================================================
# GVS functions
# ===========================================================
function open_GVS(moog_port; param = GVS, disp_INFO = true)
  println("[ INFO: setting up GVS ]")
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_OPEN_TRANSD, @sprintf("%s,%d,%s,%f",param.device, param.subdeviceNb, param.ports, param.frequency))

  while(true)
    recv=MOOG.loop(moog_port)

    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_TRANSD_NAK)
        error("[ INFO: Moog did not ack GVS ($recv[3]) ]")
      elseif(recv[2]==MOOG.MSG_AV_TRANSD_ACK)
        if disp_INFO
          println("[ INFO: GVS Acknowledged ]")
        end
        return
      else
        if disp_INFO
          println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
        end
      end
    end
  end
end

function play_GVS(moog_port; stimulus, disp_INFO = true)
  println(stimulus.name)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_TRANSD_PLAY,@sprintf("%d,%s,%f,%d", stimulus.channelNb, stimulus.name, stimulus.order, stimulus.reps))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_TRANSD_ACK)
        if disp_INFO
          println("[ INFO: AV_TRANSD_ACK ]")
        end
        return
      elseif(recv[2]==MOOG.MSG_AV_TRANSD_NAK)
        error("[ INFO: nack TRANSD $(recv) ]")
      end
    end
  end
end

function stop_GVS(moog_port; stimulus, disp_INFO = true)
  MOOG.sendmsg(moog_port, MOOG.MSG_AV_TRANSD_STOP,@sprintf("%d", stimulus.channelNb))

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_AV_TRANSD_CHANNEL_IDLE)
        if disp_INFO
          println("[ INFO: Channel $(stimulus.channelNb) has stopped transmitting and is now IDLE ]")
        end
        return
      end
    end
  end
end
