# ===========================================================
# LONG SESSION (part of the MoogCom module)
# ===========================================================
# included functions:
# - login_long
# - define_long_seq
# - run_long
#
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================

function login_long(moog_port, param, BTN, disp_INFO = true)
  #ListofBTN = checkBTN(BTN)

  # send login request to MOOG
  if moog_port.sts1!=MOOG.SSTATE_OFF
    error("must be in off-state in order to login")
  end

  MOOG.sendmsg(moog_port,MOOG.MSG_LOGIN,@sprintf("%d,%d,%s,%f,%s",MOOG.SESSION_LONG_SEQ,           moog_port.remport, param.mode, param.start_height, join(BTN.BtnLabels, ",")))

  # process the MOOG's response to login or engage request
  while(true)
    recv=MOOG.loop(moog_port)

    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_LOGIN_NAK)
        error("Moog did not ack login ($recv[3])")
      elseif(recv[2]==MOOG.MSG_LOGIN_ACK)
        if disp_INFO
          println("[ INFO: Session is defined ($recv[3]) ]")
        end
        moog_port.wait_for_idle=true
      else
        if disp_INFO
          println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
        end
      end
    end
    # engage if Login_ACK AND engage==true
    if(moog_port.wait_for_idle==true && moog_port.sts1==MOOG.SSTATE_IDLE)
      return
    end
  end
end

function define_long_seq(moog_port, seq, param, disp_INFO = false)

  # 400: name of the sequence / max actuator motion speed / center of rotation
  MOOG.sendmsg(moog_port, MOOG.MSG_BEGIN_LONG_SEQ, @sprintf("%s,%f,%f,%f,%f",
                                                            seq.name,
                                                            seq.motions[1].max_actuator_speed,
                                                            seq.motions[1].rotcenter_heave,
                                                            seq.motions[1].rotcenter_surge,
                                                            seq.motions[1].rotcenter_lateral))
  for motion in seq.motions

    # 401:
    MOOG.sendmsg(moog_port, MOOG.MSG_LONG_SEQ_STEP, @sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                                             motion.dof[1],motion.dof[2],
                                                             motion.dof[3]+param.start_height,
                                                             motion.dof[4],motion.dof[5],
                                                             motion.dof[6],motion.time,
                                                             motion.law))
  end

  # 402 communicate that the seq definition is completed.
  MOOG.sendmsg(moog_port, MOOG.MSG_COMPLETE_LONG_SEQ, "")

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_LONG_SEQ_NAK)
        error("Moog did not ack sequence ($recv[3])")
      elseif(recv[2]==MOOG.MSG_LONG_SEQ_ACK)
        if disp_INFO
          println("[ INFO: seq accepted]")
        end
        break
      else
        if disp_INFO
          println("[ INFO: received unprocessed msg $recv[2] ($recv[3])]")
        end
      end
    end
  end
end

function run_long(moog_port, seq_name, disp_INFO = false)
  if moog_port.sts1!=MOOG.SSTATE_ENGAGED
    error("must be in ENGAGED-state in order to send motion profile")
  end

  # move to start position
  MOOG.sendmsg(moog_port, MOOG.MSG_LONG_SEQ_AT_START, seq_name)

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_LONG_SEQ_EXECUTE_NAK)
        error("Moog did not ack sequence ($recv[3])")
      elseif(recv[2]==MOOG.MSG_LONG_SEQ_READY)
        if disp_INFO
          println("[ INFO: seq $(seq_name) ready to execute]")
        end
        break
      end
    end
  end

  MOOG.sendmsg(moog_port, MOOG.MSG_LONG_SEQ_EXECUTE, "")

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_LONG_SEQ_EXECUTE_NAK)
        error("Moog did not ack sequence ($recv[3])")
      elseif(recv[2]==MOOG.MSG_LONG_SEQ_DONE)
        if disp_INFO
          println("[ INFO: seq $(seq_name) is executed]")
        end
        break
      end
    end
  end
end 
