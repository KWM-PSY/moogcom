# ===========================================================
# MODULE MoogCom
# This file contains generic function for the communication with the moog base and constants XXX. Mode specific code and stimulus sercer specific code is in separate files. The content of these files is also listed below.
# 
# included struct definitions:
# - events
#
# included function definitions:
# - engage
# - park (+ logout)
# - logout
# - session_day
# - session_postproduction
# - get_IPD
# - set_IPD
# - adjust_IPD
# - activate_HW_input
# - check_topic
#
# -------------------
# Content of "MoogCom_short_session.jl"
# -------------------
# - login_short
# - send_short
#
# -------------------
# Content of "MoogCom_long_session.jl"
# -------------------
# - login_long
# - define_long_seq
# - run_long
#
# -------------------
# Content of "MoogCom_joystick.jl"
# -------------------
# - login_joystick
# -------------------
# Content of "MoogCom_stimulus_server.jl"
# -------------------
# - open_audio
# - play_audio
# - open_image
# - display_image
# - display_color
# - display_draw
# - display_hide
# - open_GVS
# - play_GVS
#
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================

module MoogCom
using MOOG,Dates,Printf,Exp_helper 
using Base: @kwdef

include("MoogCom_short_session.jl")
include("MoogCom_long_session.jl")
include("MoogCom_joystick.jl")
include("MoogCom_stimulus_server.jl")
include("MoogCom_git.jl")

# ===========================================================
# constant definitions
# ===========================================================
const STATE_LOGGING_IN = 0
const STATE_ENGAGING = 1
const STATE_ENGAGED = 2
const STATE_EXEC_TASK = 3
const STATE_PARKING = 4

# ===========================================================
# struct definitions
# ===========================================================
# the events-struct stores the information on what happens 
# during short-sessions (e.g. button presses)
@kwdef struct events
  type::Array{String}
  parameters::Array{Any}
  scheduled::Array{Float64}
  executed::Array{Float64}
end

# ===========================================================
# function definitions
# ===========================================================
function engage(moog_port)
  if moog_port.sts1!=MOOG.SSTATE_IDLE
    error("must be in IDLE-state in order to engage")
  end
  MOOG.sendmsg(moog_port,MOOG.MSG_ENGAGE,"")
  while(true)
    recv=MOOG.loop(moog_port)
    if moog_port.sts1 == MOOG.SSTATE_ENGAGED
      return
    end
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_ENGAGE_NAK)
        error("Moog did not ack engagement ($recv[3])")
      end
    end
    #    elseif(recv[2]==MOOG.MSG_ENGAGE_DONE)
    #        println("[ INFO: engage done]")
    #        return
    #    end
    #end
  end
end

function park(moog_port; mailto = nothing, report_resolution = 100, do_logout = true, disp_INFO = true)
  if moog_port.sts1!=MOOG.SSTATE_ENGAGED
    error("must be in ENGAGED-state in order to park=========> $(moog_port.sts1)")
  end
  MOOG.sendmsg(moog_port,MOOG.MSG_PARK,"")

  while(true)
    if(moog_port.sts1==MOOG.SSTATE_IDLE)
      break
    end
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if disp_INFO
        #println("[ INFO: Parking: Received unprocessed msg $recv[2] ($recv[3])]")
      end
    end
  end


  if do_logout
    logout(moog_port, mailto = mailto, report_resolution = report_resolution, disp_INFO = disp_INFO)
  end
end

function logout(moog_port; mailto = nothing, report_resolution = 100, disp_INFO = true)

  if(mailto!=nothing)
    reportmsg = string(report_resolution)*","*mailto
  else
    reportmsg = string(report_resolution)
  end

  MOOG.sendmsg(moog_port,MOOG.MSG_LOGOUT,reportmsg)

  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2]==MOOG.MSG_LOGOUT_ACK)
        if disp_INFO
          println("[ INFO: Moog  ack logout ($recv[3])]")
        end
        return
      elseif(recv[2]==MOOG.MSG_LOGOUT_NAK)
        error("NAK logout")
      end
    end
  end
end

function session_day(dayofinterest)
  moog_port = MOOG.Port()
  msg2send = @sprintf("%d,%s", moog_port.remport, dayofinterest)
  println("=====> $msg2send")
  MOOG.sendmsg(moog_port, MOOG.MSG_LIST_SESSIONS, msg2send)

  while(true)
    recv = MOOG.loop(moog_port)
    println(recv)
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_LIST_SESSIONS_SESSION)
        println("[ SESSION: ($recv[3]) ($recv[4]) ($recv[5])]")
      elseif(recv[2] == MOOG.MSG_LIST_SESSIONS_DONE)
        MOOG.close(moog_port)
        return
      elseif(recv[2] == MOOG.MSG_LIST_SESSIONS_NAK)
        MOOG.close(moog_port)
        error("NAK request $(recv)")
      end
    end
  end
end

function session_postproduction(sessionofinterest, resolution, mailadd)
  if(typeof(sessionofinterest == String)	)
    moog_port = MOOG.Port()
    msg2send = @sprintf("%d,%s,%d,%s", moog_port.remport, sessionofinterest, resolution, mailadd)
    println("=======> $msg2send")
    MOOG.sendmsg(moog_port, MOOG.MSG_POST_PRODUCE, msg2send )

    while(true)
      recv = MOOG.loop(moog_port)
      println(recv)
      if(isa(recv,Tuple))
        if(recv[2] == MOOG.MSG_POST_PRODUCE_DONE)
          MOOG.close(moog_port)
          return
        elseif(recv[2] == MOOG.MSG_POST_PRODUCE_NAK)
          # if file is too big for mail get it by scp
          # scp moog@192.168.1.200:/moog_base/sessions/2020/03/16/141542/*post_prod* .
          MOOG.close(moog_port)
          error("NAK request $(recv)")
        end
      end
    end
  else
    error("sessionofinterest has to be of type String, not $typeof(sessionofinterst)")
  end
end

#function get_IPD(moog_port, mode)
#    if mode == "EFFE"
#        MOOG.sendmsg(moog_port, MOOG.MSG_EFFE_GET_IPD)
#    end
#
#    if mode == "MACACO"
#        MOOG.sendmsg(moog_port, string(MOOG.MSG_MACACO_GET_IPD))
#    end
#
#    while(true)
#        recv=MOOG.loop(moog_port)
#        if(isa(recv,Tuple))
#            IPD = recv
#            println("==============>$(recv)")
#            return IPD
#        end
#    end
#end


function set_IPD(moog_port, value, mode)
  if mode == "EFFE"
    MOOG.sendmsg(moog_port, MOOG.MSG_EFFE_SET_IPD, @sprintf("%f", value))
  end
  if mode == "EFFE2"
    MOOG.sendmsg(moog_port, MOOG.MSG_EFFE2_SET_IPD, @sprintf("%f", value))
  end
  if mode == "MACACO"
    MOOG.sendmsg(moog_port, MOOG.MSG_MACACO_SET_IPD, @sprintf("%f", value))
  end

  if mode == "MASTER"
    MOOG.sendmsg(moog_port, MOOG.MSG_AV_STILLS_SET_IPD, @sprintf("%f", value))
  end
end

function adjust_IPD(moog_port, dist = 92, stepsize = 1, mode = "EFFE")
  println("hit h or g to adjust the inter pupillar distance. Press e to exit")

  while true
    #println(typeof(stepsize))
    inp = readline()
    if inp == "e"
      return dist
    elseif inp == "h"
      dist += stepsize
    elseif inp == "g"
      dist -= stepsize
    end
    MoogCom.set_IPD(moog_port, dist, mode)
    println(dist)
  end
end

function activate_HW_input(moog_port, HW, dispINFO = true)
  fullmsg = ""
  for x = 1:size(HW,1)
    if typeof(HW[x]) == Exp_helper.Gamepad
      msg = "gamepad" * string(x) * "|gamepad";
      for b = 1:HW[x].numberBtn
        msg = msg * "|" * HW[x].BtnLabels[b]
      end
    elseif typeof(HW[x]) == Exp_helper.Xsense
      msg = "IMU|xsense"
    elseif typeof(HW[x]) == Exp_helper.spnav
      msg = "spacemouse|spnav"
    else
      error("unknown hardware component requested")
    end

    if x == 1
      fullmsg = msg
    else
      fullmsg = fullmsg * "," * msg
    end
  end
  if dispINFO
    println(fullmsg)
  end
  MOOG.sendmsg(moog_port,MOOG.MSG_ACTIVATE_HW_INPUT,fullmsg)

  # process the MOOG's response to check if hardware was ACK
  while(true)
    recv=MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      println(recv)
      if(recv[2]==MOOG.MSG_HW_INPUT_NAK)
        error("Moog did not ack the requested hardware ($recv[3])")
      elseif(recv[2]==MOOG.MSG_HW_INPUT_ACK)
        if dispINFO
          println("[ INFO: Hardware Acknowledged ($recv[3]) ]")
        end
        return
      else
        println("[ INFO: Login loop: received unprocessed msg $recv[2] ($recv[3])]")
      end
    end
  end
end

function check_topic(moog_port, expected_topic, dispINFO = true)

  MOOG.sendmsg(moog_port, MOOG.MSG_GET_TOPIC, "")

  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if dispINFO
        println(recv[3])
      end
      if(recv[2] == MOOG.MSG_TOPIC)
        if recv[3] == [expected_topic]
          return
        else
          error("Wrong topic selected on server! Expected '$expected_topic' found '$(recv[3][1])' instead!")
        end
      end
    end
  end
end

end # end module
