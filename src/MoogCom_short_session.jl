# ===========================================================
# SHORT SESSION (part of the MoogCom module)
# ===========================================================
# included functions:
# - login_short
# - send_short
# - send_short_media
# - send_short_events
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
function login_short(moog_port; buttons, parameters, disp_INFO = true)
  # send login request to MOOG
  if moog_port.sts1!=MOOG.SSTATE_OFF
    error("[ ERROR: must be in off-state in order to login ]")
  end
  MOOG.sendmsg(moog_port,MOOG.MSG_LOGIN,@sprintf("%d,%d,%s,%f,%s",MOOG.SESSION_SHORT_SEQ,          moog_port.remport, parameters.mode, parameters.start_height, join(buttons.BtnLabels, ",")))

  # process the MOOG's response to login or engage request
  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_LOGIN_NAK)
        error("[ ERROR: Moog did not ack login ($recv[3]) ]")
      elseif(recv[2] == MOOG.MSG_LOGIN_ACK)
        if disp_INFO
          println("[ INFO: Session is defined ]")
        end
        moog_port.wait_for_idle=true
        break
      else
        #println("Login loop: received unprocessed msg $recv[2] ($recv[3])")
      end
    end
  end

  # open audio if Login_ACK AND parameters.stim_audio==true
  if parameters.stim_audio
    open_audio(moog_port, parameters, disp_INFO)
  end
end

function send_short(moog_port; motion_stimulus, parameters, input_device = "gamepad", exiton = "motion", disp_INFO = true)
  # check if moog is engaged
  if moog_port.sts1!=MOOG.SSTATE_ENGAGED
    error("[ ERROR: must be in ENGAGED-state in order to send motion profile ]")
  end

  # announce motion profile
  MOOG.sendmsg(moog_port, MOOG.MSG_BEGIN_SHORT_SEQ, "$(motion_stimulus[1].max_actuator_speed),     $(motion_stimulus[1].rotcenter_heave),$(motion_stimulus[1].rotcenter_surge),$(motion_stimulus[1].rotcenter_lateral)")
  # if start_sound == true | stop_sound == true
  # test if audio server is open

  for motion in motion_stimulus
    # send motion profile
    MOOG.sendmsg(moog_port, MOOG.MSG_SHORT_SEQ_STEP,@sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                                             motion.dof[1],motion.dof[2],
                                                             motion.dof[3]+parameters.start_height,
                                                             motion.dof[4],motion.dof[5],
                                                             motion.dof[6],motion.time,
                                                             motion.law))
  end

  # close sequence transmission
  MOOG.sendmsg(moog_port, MOOG.MSG_COMPLETE_SHORT_SEQ, "")

  sequence_done = false
  pressed_buttons_name = String[]
  pressed_buttons_time = Int64[]
  motion_start = Int[]

  # check if short_seq_ack
  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_SHORT_SEQ_NAK)
        error("[ ERROR: Moog did not ack sequence ($recv[3]) ]")
      elseif(recv[2] == MOOG.MSG_SHORT_SEQ_ACK)
        push!(motion_start, recv[1])
        if disp_INFO
          println("[ INFO: sequence accepted ]")
        end
        break
      end
    end
  end

  # check if buttons were pressed
  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv, Tuple))
      # inputdevice == buttons
      if parameters.input_device == "buttons"
        if(recv[2] == MOOG.MSG_DPIN_CHANGE)
          if(recv[3][2] == "1") && (parse(Int, recv[3][3]) > motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", string(recv[3][1]), "- was pressed ]")
            end
            push!(pressed_buttons_name, recv[3][1])
            push!(pressed_buttons_time, parse(Int, recv[3][3]))
          end
        end
        #inputdevice == gamepad
      elseif parameters.input_device == "gamepad"
        if(recv[2] == MOOG.MSG_HW_INPUT_EVENT)
          if(recv[3][5] == "1") && (parse(Int, recv[3][1]) > motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", string(recv[3][1]), "- was pressed ]")
            end
            push!(pressed_buttons_name, recv[3][4])
            push!(pressed_buttons_time, parse(Int, recv[3][1]))
          end
        end
      end
      if(recv[2] == MOOG.MSG_SHORT_SEQ_DONE)
        if disp_INFO
          println("[ INFO: sequence done ]")
        end
        sequence_done = true
      end
      if(sequence_done)
        if(exiton == "motion")
          if isempty(pressed_buttons_name)
            return motion_start, nothing, nothing
          else
            return motion_start, pressed_buttons_name, pressed_buttons_time
          end
        elseif(exiton == "button")
          if(length(pressed_buttons_name) > 0)
            return motion_start, pressed_buttons_name, pressed_buttons_time
          end
        end
      end
    end
  end
end




# ========================
# send short media
# This function allows to interact with media (screens, GVS, audio, etc) while a motion is executed.
# ========================
function send_short_media(moog_port; motion_stimulus, parameters, media = [], input_device = "gamepad", exiton = "motion", disp_INFO = true)
  # check if moog is engaged
  if moog_port.sts1!=MOOG.SSTATE_ENGAGED
    error("[ ERROR: must be in ENGAGED-state in order to send motion profile ]")
  end

  # announce motion profile
  MOOG.sendmsg(moog_port, 
    MOOG.MSG_BEGIN_SHORT_SEQ, 
    "$(motion_stimulus[1].max_actuator_speed),     
    $(motion_stimulus[1].rotcenter_heave),
    $(motion_stimulus[1].rotcenter_surge),
    $(motion_stimulus[1].rotcenter_lateral)")
  # if start_sound == true | stop_sound == true
  # test if audio server is open

  for motion in motion_stimulus
    # send motion profile
    MOOG.sendmsg(moog_port, MOOG.MSG_SHORT_SEQ_STEP,@sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                                             motion.dof[1],motion.dof[2],
                                                             motion.dof[3]+parameters.start_height,
                                                             motion.dof[4],motion.dof[5],
                                                             motion.dof[6],motion.time,
                                                             motion.law))
  end

  # close sequence transmission
  MOOG.sendmsg(moog_port, MOOG.MSG_COMPLETE_SHORT_SEQ, "")

  sequence_done = false
  pressed_buttons_name = String[]
  pressed_buttons_time = Int64[]
  motion_start = Int[]

  # check if short_seq_ack
  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_SHORT_SEQ_NAK)
        error("[ ERROR: Moog did not ack sequence ($recv[3]) ]")
      elseif(recv[2] == MOOG.MSG_SHORT_SEQ_ACK)
        push!(motion_start, recv[1])
        events = MoogCom.events(type = ["motion started"], 
                                parameters = [],
                                sheduled = [NaN],
                                executed = [recv[1]]) 

        if disp_INFO
          println("[ INFO: sequence accepted ]")
          println(events)
        end
        break
      end
    end
  end

  motion_start_time = time()
  # check if buttons were pressed
  while(true)
    recv = MOOG.loop(moog_port)

    # check for button presses
    if(isa(recv, Tuple))
      # inputdevice == buttons
      if parameters.input_device == "buttons"
        if(recv[2] == MOOG.MSG_DPIN_CHANGE)
          if(recv[3][2] == "1") && 
            (parse(Int, 
                   recv[3][3]) > motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", 
                      string(recv[3][1]), 
                      "- was pressed ]")
            end
            push!(events.type, "button press")
            push!(events.parameters, recv[3][3])
            push!(events.scheduled, NaN)
            push!(events.executed, parse(Float64,recv[3][1]))

            push!(pressed_buttons_name, 
                  recv[3][1])
            push!(pressed_buttons_time, 
                  parse(Int, recv[3][3]))
          end
        end
        #inputdevice == gamepad
      elseif parameters.input_device == "gamepad"
        if(recv[2] == MOOG.MSG_HW_INPUT_EVENT)
          if(recv[3][5] == "1") && 
            (parse(Int, recv[3][1]) > 
             motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", 
                      string(recv[3][1]), 
                      "- was pressed ]")
            end
            push!(events.type, "button press")
            push!(events.parameters, recv[3][4])
            push!(events.scheduled, NaN)
            push!(events.executed, parse(Float64,recv[3][1]))

            push!(pressed_buttons_name, recv[3][4])
            push!(pressed_buttons_time, 
                  parse(Int, recv[3][1]))
          end
        end
      end

      # check if motion is done
      if(recv[2] == MOOG.MSG_SHORT_SEQ_DONE)
        if disp_INFO
          println("[ INFO: sequence done ]")
        end
        sequence_done = true
      end

      # check if trial ends on motion or button press
      if(sequence_done)
        if(exiton == "motion")
          if isempty(pressed_buttons_name)
            return events
          end
        elseif(exiton == "button")
          if(length(pressed_buttons_name) > 0)
            return events 
          end
        end
      end
    end
    # check if there is any media-action in the cue and if it is time to do something	
    if ~isempty(media)
      time_since_start = time() - motion_start_time
      if time_since_start >= media[1]
        if media[3] == "image"
          MoogCom.display_image(moog_port, media[4], media[5], media[6])		
          if length(media) > media[2]
            media = media[media[2]+1:end]
          else
            media = [] 
          end
        end
      end
    end
  end
end


# ========================
# send short events
# This function allows to interact with media (screens, GVS, audio, etc) while a motion is executed.
# ========================
function send_short_events(moog_port; motion_stimulus, parameters, events_in = [], input_device = "gamepad", exiton = "motion", disp_INFO = true)
  # get current time as reference for all events in this trial
  trial_start_time = time()

  if events_in == []
    event_in_counter = 0
  else
    event_in_counter = 1
  end
  # check if moog is engaged
  if moog_port.sts1!=MOOG.SSTATE_ENGAGED
    error("[ ERROR: must be in ENGAGED-state in order to send motion profile ]")
  end

  # announce motion profile
  MOOG.sendmsg(moog_port, 
    MOOG.MSG_BEGIN_SHORT_SEQ, 
    "$(motion_stimulus[1].max_actuator_speed),     
    $(motion_stimulus[1].rotcenter_heave),
    $(motion_stimulus[1].rotcenter_surge),
    $(motion_stimulus[1].rotcenter_lateral)")
  # if start_sound == true | stop_sound == true
  # test if audio server is open

  for motion in motion_stimulus
    # send motion profile
    MOOG.sendmsg(moog_port, MOOG.MSG_SHORT_SEQ_STEP,@sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                                             motion.dof[1],motion.dof[2],
                                                             motion.dof[3]+parameters.start_height,
                                                             motion.dof[4],motion.dof[5],
                                                             motion.dof[6],motion.time,
                                                             motion.law))
  end

  # close sequence transmission
  MOOG.sendmsg(moog_port, MOOG.MSG_COMPLETE_SHORT_SEQ, "")

  sequence_done = false
  pressed_buttons_name = String[]
  pressed_buttons_time = Int64[]
  motion_start = Int[]


  events_out  = []
  motion_start_time = NaN
  # check if short_seq_ack
  while(true)
    recv = MOOG.loop(moog_port)
    if(isa(recv,Tuple))
      if(recv[2] == MOOG.MSG_SHORT_SEQ_NAK)
        error("[ ERROR: Moog did not ack sequence ($recv[3]) ]")
      elseif(recv[2] == MOOG.MSG_SHORT_SEQ_ACK)
        push!(motion_start, recv[1])
        motion_start_time = time() - trial_start_time 
        events_out = MoogCom.events(type = ["motion started"], 
                                    parameters = [""],
                                    scheduled = [NaN],
                                    executed = [motion_start_time]) 
        if disp_INFO
          println("[ INFO: sequence accepted ]")
          println(events)
        end
        break
      end
    end
  end

  # check if buttons were pressed
  while(true)
    recv = MOOG.loop(moog_port)

    # check for button presses
    if(isa(recv, Tuple))
      # inputdevice == buttons
      if parameters.input_device == "buttons"
        if(recv[2] == MOOG.MSG_DPIN_CHANGE)
          if(recv[3][2] == "1") && 
            (parse(Int, recv[3][3]) > motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", 
                      string(recv[3][1]), 
                      "- was pressed ]")
            end
            push!(events_out.type, "button press")
            push!(events_out.parameters, recv[3][3])
            push!(events_out.scheduled, NaN)
            push!(events_out.executed, time() - trial_start_time)

            push!(pressed_buttons_name, recv[3][4])
            push!(pressed_buttons_time, 
                  parse(Int, recv[3][1]))


          end
        end
        #inputdevice == gamepad
      elseif parameters.input_device == "gamepad"
        if(recv[2] == MOOG.MSG_HW_INPUT_EVENT)
          if(recv[3][5] == "1") && 
            (parse(Int, recv[3][1]) > 
             motion_start[1])
            if disp_INFO
              println("[ BTN: Button -", 
                      string(recv[3][1]), 
                      "- was pressed ]")
            end
            push!(events_out.type, "button press")
            push!(events_out.parameters, recv[3][3])
            push!(events_out.scheduled, NaN)
            push!(events_out.executed, time() - trial_start_time)

            push!(pressed_buttons_name, recv[3][4])
            push!(pressed_buttons_time, 
                  parse(Int, recv[3][1]))
          end
        end
      end

      # check if motion is done
      if(recv[2] == MOOG.MSG_SHORT_SEQ_DONE)
        if disp_INFO
          println("[ INFO: sequence done ]")
        end
        sequence_done = true
      end

      # check if trial ends on motion or button press
      if(sequence_done)
        if(exiton == "motion")
          if isempty(pressed_buttons_name)
            return events_out
          end
        elseif(exiton == "button")
          if(length(pressed_buttons_name) > 0)
            return events_out
          end
        end
      end
    end
    # check if there is any event-action in the cue and if it is time to do something	
    if event_in_counter > 0 
      time_since_start = time() - trial_start_time 
      if time_since_start >= events_in.scheduled[event_in_counter]
        if events_in.type[event_in_counter] == "image"
          p = split(events_in.parameters[event_in_counter], "|")
          MoogCom.display_image(moog_port, p[1], parse(Int64, p[2]))		

          if length(events_in.type) > event_in_counter
            event_in_counter += 1
          else
            event_in_counter = -1 
          end
        end
      end
    end
  end
end
