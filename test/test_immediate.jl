# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================

using MoogCom,Random,Printf,Dates,MOOG

# emulator mode
SERVER_MOOG_ADDRESS = MY_EMULATOR_ADDRESS

# moog mode
#SERVER_MOOG_ADDRESS = MY_SERVER_ADDRESS

# =============================
# login & engage

#MoogCom.session_param()

mode = "D"
start_height  = -0.2
moog_port = MOOG.Port()

nb_buttons = 1
button_name = ["BUUU"]


rotcenter_heave = start_height
max_actuator_speed = 0.15
rotcenter_surge = 0
rotcenter_lateral = 0
# =============================
# generate motion profiles


lat = MoogCom.Movement([0.05, 0.0, 0.0, 0.0, 0.0, 0.0], 5.0, 'S')
hea = MoogCom.Movement([0.0, 0.05, 0.0, 0.0, 0.0, 0.0], 5.0, 'S')
sur = MoogCom.Movement([0.0, 0.0, 0.05, 0.0, 0.0, 0.0], 5.0, 'S')
yaw = MoogCom.Movement([0.0, 0.0, 0.0, 0.05, 0.0, 0.0], 5.0, 'S')
pit = MoogCom.Movement([0.0, 0.0, 0.0, 0.0, 0.05, 0.0], 5.0, 'S')
rol = MoogCom.Movement([0.0, 0.0, 0.0, 0.0, 0.0, 0.05], 5.0, 'S')
mid = MoogCom.Movement([0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 5.0, 'S')

test_seq = [lat, mid, hea, mid, sur, mid, yaw, mid, pit, mid, rol, mid]

MoogCom.login_short(moog_port, 
                    MoogCom.Button(1,button_name), 
                    mode, 
                    start_height)

MoogCom.engage(moog_port)

MoogCom.send_short(moog_port, 
                   test_seq,
                   start_height, 
                   max_actuator_speed, 
                   rotcenter_heave, 
                   rotcenter_surge, 
                   rotcenter_lateral)



# =============================
# park
MoogCom.park(moog_port, nothing, 20)
