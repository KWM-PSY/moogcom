# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using MoogCom, Random, Printf, Dates, MOOG, Exp_helper

Exp_helper.experiment_banner(name = "test_long")

# =============================
# login & engage
moog_port = MOOG.Port()

# define number and names of buttons
BTN = Exp_helper.Button(numberBtn = 8, 
                        BtnLabels = ["right"; "left"; "2"; "3"; "4"; "5"; "6"; "7"])

# define mode and start height
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false,
                               )

# define motion profiles
rol = Exp_helper.Movement(dof = [0.05, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = -0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0,
                          sound_at_start = false,
                          sound_at_stop = false)

mid = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = -0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0,
                          sound_at_start = false,
                          sound_at_stop = false)

short_mid = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 0.01,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = -0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0,
                          sound_at_start = false,
                          sound_at_stop = false)

sequences = [Exp_helper.Sequence("roll_pos", [short_mid, rol, mid])]

MoogCom.login_long(moog_port, 
                   param,
                   BTN)

# define each sequence inside "sequences"
for seq in sequences
    MoogCom.define_long_seq(moog_port, seq, param, true)
end

MoogCom.engage(moog_port)

MoogCom.run_long(moog_port,"roll_pos")

# =============================
# park
MoogCom.park(moog_port, 
             mailto = nothing, 
             report_resolution = 20,
             disp_INFO = true)
