# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using MOOG, MoogCom, Exp_helper 
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false)

mode = "D"
start_height  = -0.2
moog_port = MOOG.Port()
mailto = ""
report_resolution = 20
const_velocity = 0.1
rot_center = [0.0,0.0,0.0]

MoogCom.login_joystick(moog_port, mailto, report_resolution, const_velocity, rot_center)
