# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================



using MoogCom, Random, Printf, Dates, MOOG, Exp_helper

Exp_helper.experiment_banner(name = "test_sound")

# =============================
# login & engage
moog_port = MOOG.Port()

# define mode and start height
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false,
                               )


# define auditive stimulus
# audio file must first be imported via stimuli web server (see Manual)
sound = Exp_helper.Audio(filename = "beep",
                         channel = 0,
                         volume = 1,
                         repetitions = 0)

BTN = Exp_helper.Button(numberBtn = 8, 
                        BtnLabels = ["right"; "left"; "2"; "3"; "4"; "5"; "6"; "7"])


MoogCom.login_short(moog_port, 
                    buttons = BTN,
                    parameters = param,
                    disp_INFO = true)

MoogCom.open_audio(moog_port, 
		   n_channels = 2,
		   disp_INFO = true)
for i = 1:3
    MoogCom.play_audio(moog_port, sound)
    sleep(3)
end



# =============================
# logout
MoogCom.logout(moog_port,
               mailto = nothing, 
               report_resolution = 20,
               disp_INFO = true)

