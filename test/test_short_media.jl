# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using MoogCom, Random, Printf, Dates, MOOG, Exp_helper

# =============================
# prepare buttons & parameters
moog_port = MOOG.Port()
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               input_device = "buttons",
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false)

BTN = Exp_helper.Button(numberBtn = 2, 
                        BtnLabels = ["right", "left"])

gamepad = Exp_helper.Gamepad(numberBtn = 2,
                             BtnLabels = ["circle", "square"])

# =============================
# prepare motion profiles
rol = Exp_helper.Movement(dof = [0.05, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = -0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0)

rol2 = Exp_helper.Movement(dof = [0.05, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = 0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0)

mid = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = -0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0)
mid2 = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
                          time = 2.0,
                          law = "S",
                          max_actuator_speed = 0.15,
                          rotcenter_heave = 0.2,
                          rotcenter_surge = 0.0,
                          rotcenter_lateral = 0.0)


center = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
			     time = 0,
			     law = "S",
			     max_actuator_speed = 0.15,
			     rotcenter_heave = -0.2,
			     rotcenter_surge = 0.0,
			     rotcenter_lateral = 0.0)
	
center2 = Exp_helper.Movement(dof = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
			     time = 0,
			     law = "S",
			     max_actuator_speed = 0.15,
			     rotcenter_heave = 0.2,
			     rotcenter_surge = 0.0,
			     rotcenter_lateral = 0.0)		     
still = Exp_helper.Image()
# =============================
# login & engage

MoogCom.login_short(moog_port, 
                    buttons = BTN,
                    parameters = param,
                    disp_INFO = true)

MoogCom.open_image(moog_port,
                    still,
                    true)

MoogCom.engage(moog_port)
# only if gamepad
#MoogCom.activate_HW_input(moog_port, 
#                          HW = [gamepad],
#                          dispINFO = true)

# =============================
# start experiment
Exp_helper.experiment_banner(name = "/ test_short", 
			     ask = false)



MoogCom.send_short_media(moog_port;
                         motion_stimulus = [center],
                         parameters = param,
                         exiton = "motion",
                         disp_INFO = true) 

events = MoogCom.send_short_media(moog_port;
				motion_stimulus = [rol],
				parameters = param,
				exiton = "motion",
				disp_INFO = true) 

#test_media = MoogCom.events(type = "image",
#	       parameters = ["white",0,false],
#	       sheduled = 0.5,
#	       executed = NaN)		

events = MoogCom.send_short_media(moog_port;
                         motion_stimulus = [mid],
                         parameters = param,
			 media = [0.5,
				  6,
				  "image",
				  "white", 
				  0, 
				  false,
				  1.5,
				  6,
				  "image",
				  "whitefix",
				  0,
				  false
				  ],
                         exiton = "motion",
                         disp_INFO = true) 
println("====> ", events)

MoogCom.send_short_media(moog_port;
                         motion_stimulus = [center2],
                         parameters = param,
                         exiton = "motion",
                         disp_INFO = true) 

MoogCom.send_short_media(moog_port;
				motion_stimulus = [rol2],
				parameters = param,
				exiton = "motion",
				disp_INFO = true) 

MoogCom.send_short_media(moog_port;
		   motion_stimulus = [mid2],
		   parameters = param,
		   exiton = "motion",
		   disp_INFO = true) 
# =============================
# park
MoogCom.park(moog_port, 
             mailto = nothing, 
             report_resolution = 20,
             disp_INFO = true)
