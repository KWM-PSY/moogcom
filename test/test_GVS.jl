# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using MoogCom, Random, Printf, Dates, MOOG, Exp_helper


# =============================
# login & engage
moog_port = MOOG.Port()

# define mode and start height
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false)


# define auditive stimulus
GVS = Exp_helper.GVS(name = "para7",
                     device = "TRANS-VIRT",
                     subdeviceNb = 1,
                     ports = "0/0.0/5.0|6/0.0/10.0",
                     frequency = 60.0,
                     channelNb = 1,
                     order = 1,
                     reps = 0)

BTN = Exp_helper.Button(numberBtn = 8, 
                        BtnLabels = ["right"; "left"; "2"; "3"; "4"; "5"; "6"; "7"])


MoogCom.login_short(moog_port, 
                    buttons = BTN,
                    parameters = param,
                    disp_INFO = true)

MoogCom.open_GVS(moog_port, 
                 param = GVS,
                 disp_INFO = true)

Exp_helper.experiment_banner(name = "test_GVS")

MoogCom.play_GVS(moog_port, stimulus = GVS, disp_INFO = true)

# =============================
# logout
MoogCom.logout(moog_port,
               mailto = nothing, 
               report_resolution = 20,
               disp_INFO = true)

