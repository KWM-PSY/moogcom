# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using MoogCom, Random, Printf, Dates, MOOG, Exp_helper

# =============================
# prepare buttons & parameters
moog_port = MOOG.Port()
param = Exp_helper.Login_param(mode = "D",
                               start_height = -0.2,
                               input_device = "gamepad",
                               visor = "VIVE",
                               stim_audio = false,
                               n_channels = 4,
                               stim_visual = false)

BTN = Exp_helper.Button(numberBtn = 2, 
                        BtnLabels = ["right"; "left"])

gamepad = Exp_helper.Gamepad(numberBtn = 2,
                             BtnLabels = ["circle"; "square"])

spnav = Exp_helper.Spnav(numberBtn = 2,
                         BtnLabels = ["left", "right"])
xsense = true

# =============================
# login & engage

MoogCom.login_short(moog_port, 
                    buttons = BTN,
                    parameters = param,
                    disp_INFO = true)

MoogCom.engage(moog_port)
MoogCom.activate_HW_input(moog_port, 
                          HW = [gamepad, spnav, xsense],
                          dispINFO = true)
sleep(5)
# =============================
# park
MoogCom.park(moog_port, 
             mailto = nothing, 
             report_resolution = 20,
             disp_INFO = true)
